package efi.th_nuernberg.de.smartmirrorapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Isabella on 01.05.2017.
 */

public class ProfileSwitch extends Activity {

    private WebView webView;
    //private String urlRemoteControl ="http://192.168.188.36:8080/remote.html";

    private String urlRemoteControl = "http://192.168.188.30:8080/remote?action=NOTIFICATION&notification=CURRENT_PROFILE&payload=%22felix%22";
    // http://192.168.188.30:8080/remote?action=NOTIFICATION&notification=CURRENT_PROFILE&payload=%22felix%22"
    private static final  int A_LENGTH = 3;

    private String[] urlProfiles = new String [A_LENGTH];


    private int shownProf = 0;
    private ImageView imageView;

    //On Swipe Left and Right

    private float x1, x2;
    static final int MIN_DISTANCE = 150;
    private final String SMART_MIRROR_URL = "http://192.168.188.30:8080";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiles);

        urlProfiles[0] = SMART_MIRROR_URL;
        urlProfiles[1] = "http://192.168.188.30:8080/remote?action=NOTIFICATION&notification=CURRENT_PROFILE&payload=%22default%22";
        urlProfiles[2] = "http://192.168.188.30:8080/remote?action=NOTIFICATION&notification=CURRENT_PROFILE&payload=%22default%22";


        webView = (WebView) findViewById(R.id.webView1);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new ProfileSwitch.myWebClient2());
        webView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            x1 = event.getX();
                            break;
                        case MotionEvent.ACTION_UP:
                            x2 = event.getX();
                            float deltaX = x2 - x1;
                            if (Math.abs(deltaX) > MIN_DISTANCE) {
                                //Toast.makeText(this, "left2right swipe", Toast.LENGTH_SHORT).show ();
                                onSwipe();
                            } else {
                                // consider as something else - a screen tap for example
                            }
                            break;
                    }
                    return ProfileSwitch.super.onTouchEvent(event);

                }
            }
        });

        //sP aufrufen
        SharedPreferences sharedPref = getSharedPreferences("profilesState", MODE_PRIVATE);
        shownProf = sharedPref.getInt("state", 0);
        showURL(urlProfiles[shownProf]);


    }


    public void onSwipe() {

        if (shownProf == A_LENGTH - 1) {
            shownProf = 0;
            Log.i(getLocalClassName(), "shownProf if: " + shownProf);
        } else {
            shownProf++;
            Log.i(getLocalClassName(), "shownProf else: " + shownProf);
        }
        showURL(urlProfiles[shownProf]);
    }

    @Override
    protected void onStop() {
        super.onStop();
        savePreferences();
    }

    @Override
    protected void onPause() {
        super.onPause();
        savePreferences();
    }

    private void savePreferences() {
        SharedPreferences settings = getSharedPreferences("profilesState", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("state", shownProf);
        editor.commit();
    }


    public void showURL(String url) {
        webView.loadUrl(url);
        if (!url.equals(SMART_MIRROR_URL))
        {
            webView.loadUrl(SMART_MIRROR_URL);
        }
        webView.requestFocus();
    }

    public class myWebClient2 extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;

        }


        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            setContentView(R.layout.activity_errorpage);
            TextView tv = (TextView) findViewById(R.id.errorText);
            tv.setText(errorCode + " " + description + " "+ urlProfiles[shownProf]);

        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){
            Log.i("onReceivedError", "HEY ERROR");

            setContentView(R.layout.activity_errorpage);
            Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Bold.ttf");
            // TextView tv =  (TextView) findViewById(R.id.textView);
            //tv.setTypeface(tf);

            TextView tv = (TextView) findViewById(R.id.errorText);
            tv.setTypeface(tf);
            tv.setText(error.toString() + " " + urlProfiles[shownProf] );
        }
    }
}