package efi.th_nuernberg.de.smartmirrorapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Isabella on 01.05.2017.
 */

public class RemoteControl extends Activity {

    private WebView webView;
    //private String urlRemoteControl ="http://192.168.188.36:8080/remote.html";

    private String urlRemoteControl = "http://192.168.188.36:8080/remote.html";
    private String[] urlProfiles = {"https://my.ohmportal.de/", "https://bitbucket.org", "https://app.asana.com", "https://facebook.de"};
    private int shownProf = 0;
    private ImageView imageView;

    //On Swipe Left and Right

    private float x1, x2;
    static final int MIN_DISTANCE = 150;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote);

        webView = (WebView) findViewById(R.id.webView1);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            x1 = event.getX();
                            break;
                        case MotionEvent.ACTION_UP:
                            x2 = event.getX();
                            float deltaX = x2 - x1;
                            if (Math.abs(deltaX) > MIN_DISTANCE) {
                                //Toast.makeText(this, "left2right swipe", Toast.LENGTH_SHORT).show ();
                                onSwipe();
                            } else {
                                // consider as something else - a screen tap for example
                            }
                            break;
                    }
                    return RemoteControl.super.onTouchEvent(event);

                }
            }
        });
        showURL(urlRemoteControl);

    }


    public void onSwipe() {
        showURL(urlProfiles[shownProf]);

        if (shownProf > urlProfiles.length -1)
            shownProf =0;
        else
            shownProf++;
    }


    public void showURL(String url) {
        webView.loadUrl(url);
        webView.requestFocus();
    }


}
