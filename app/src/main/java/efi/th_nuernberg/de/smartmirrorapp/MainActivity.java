package efi.th_nuernberg.de.smartmirrorapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends Activity implements View.OnClickListener {


    private Button btnRemoteControl;
    private Button btnHomeAssistant;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnRemoteControl = (Button) findViewById(R.id.btn_remote_control);
        btnRemoteControl.setOnClickListener(this);

        btnHomeAssistant = (Button) findViewById(R.id.btn_home_assistant);
        btnHomeAssistant.setOnClickListener(this);

        ImageView imageview = (ImageView) findViewById(R.id.imageView);
        imageview.setImageResource(R.drawable.haus_weiss);

        Button btnExit = (Button) findViewById(R.id.btn_exit);
        btnExit.setOnClickListener(this);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Bold.ttf");
        TextView tv =  (TextView) findViewById(R.id.textView);
        tv.setTypeface(tf);
        btnHomeAssistant.setTypeface(tf);
        btnRemoteControl.setTypeface(tf);
        btnExit.setTypeface(tf);
    }

    public void onClick(View v) {

        if ( v == btnRemoteControl){
            Intent i = new Intent(this, RemoteControl.class);
            startActivity(i);
        } else if (v == btnHomeAssistant)
        {
            Intent i = new Intent(this, HomeAssistant.class);
            startActivity(i);

        } else
        {
            finish();
        }

    }



}
