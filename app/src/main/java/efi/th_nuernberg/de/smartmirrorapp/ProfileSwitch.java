package efi.th_nuernberg.de.smartmirrorapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Isabella on 01.05.2017.
 */

public class RemoteControl extends Activity {

    private WebView webView;
    //private String urlRemoteControl ="http://192.168.188.36:8080/remote.html";

    private String urlRemoteControl = "http://192.168.188.36:8080/remote.html";
    private String[] urlProfiles = {"http://192.168.188.30:8080/remote?action=NOTIFICATION&notification=CURRENT_PROFILE&payload=%22felix%22", "https://bitbucket.org", "https://app.asana.com", "https://facebook.de"};
    private int shownProf = 0;
    private ImageView imageView;

    //On Swipe Left and Right

    private float x1, x2;
    static final int MIN_DISTANCE = 150;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote);

        webView = (WebView) findViewById(R.id.webView1);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new RemoteControl.myWebClient2());
        webView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            x1 = event.getX();
                            break;
                        case MotionEvent.ACTION_UP:
                            x2 = event.getX();
                            float deltaX = x2 - x1;
                            if (Math.abs(deltaX) > MIN_DISTANCE) {
                                //Toast.makeText(this, "left2right swipe", Toast.LENGTH_SHORT).show ();
                                onSwipe();
                            } else {
                                // consider as something else - a screen tap for example
                            }
                            break;
                    }
                    return RemoteControl.super.onTouchEvent(event);

                }
            }
        });
        showURL(urlRemoteControl);

    }


    public void onSwipe() {
        showURL(urlProfiles[shownProf]);
        if (shownProf > urlProfiles.length -1)
            shownProf =0;
        else
            shownProf++;
    }


    public void showURL(String url) {
        webView.loadUrl(url);
        webView.requestFocus();
    }

    public class myWebClient2 extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;

        }


        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            setContentView(R.layout.activity_errorpage);
            TextView tv = (TextView) findViewById(R.id.errorText);
            tv.setText(errorCode + " " + description + urlProfiles[shownProf]);

        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){
            Log.i("onReceivedError", "HEY ERROR");

            setContentView(R.layout.activity_errorpage);
            TextView tv = (TextView) findViewById(R.id.errorText);
            tv.setText(error.toString() + " " + urlProfiles[shownProf] );
        }
    }
}