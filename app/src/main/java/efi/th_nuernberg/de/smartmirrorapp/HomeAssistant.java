package efi.th_nuernberg.de.smartmirrorapp;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MotionEvent;
import android.webkit.WebResourceError;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Isabella on 01.05.2017.
 */

public class HomeAssistant extends Activity {

        private WebView webView;
        private String urlHomeAssistant = "https://home-assistant.io/demo/";


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_remote);

            webView = (WebView) findViewById(R.id.webView1);
            webView.getSettings().setDomStorageEnabled(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setScrollBarStyle(WebView.SCROLLBARS_INSIDE_OVERLAY);
            showURL(urlHomeAssistant);
        }

        public void showURL(String url) {
            webView.loadUrl(url);
            webView.requestFocus();
        }


    public void onReceivedError(WebView view, WebResourceError request, WebResourceError error){
        //Your code to do
        Toast.makeText(this, "Your Internet Connection May not be active Or " + error , Toast.LENGTH_LONG).show();
    }





}
